#!/bin/bash
#
#script to build everithing
# use -d for debug
# use -c for clean & build
# KREMBOT_SIM_PATH should be set to your krembot_sim directory e.g. set KREMBOT_SIM_PATH=~/krembot_sim


#make sure KREMBOT_SIM_PATH is defined
if [ -z ${KREMBOT_SIM_PATH+x} ]; then
 echo "error: KREMBOT_SIM_PATH should be set to your krembot_sim directory"
 echo "       e.g. set KREMBOT_SIM_PATH=~/krembot_sim"
 exit
fi

debug=0
clean=0

#check whether we need to clean / build for debug
if ( ([ $# -gt 0 ] && [ "$1" != "-d" ] && [ "$1" != "-c" ]) || ([ $# -gt 1 ]) ); then
 echo "================================================"
 echo "usage: use -d for clean & debug or -c for clean"
 echo "================================================"
 exit
elif ([ $# -gt 0 ] && [ "$1" == "-d" ]); then
 debug=1
 clean=1
elif ([ $# -gt 0 ] && [ "$1" == "-c" ]); then
 clean=1
fi

#build simulator
echo "=================================="
echo "build krembot_sim/argos3"
echo "=================================="
cd $KREMBOT_SIM_PATH/argos3/build_simulator || exit 255 # error if cannot cd
sudo make install
echo "=================================="
echo "done build krembot_sim/argos3"
echo "=================================="

#build krembot
echo "=================================="
echo "build krembot_sim/krembot"
echo "=================================="
cd $KREMBOT_SIM_PATH/krembot/build
if (( $clean )); then
 cd $KREMBOT_SIM_PATH/krembot
 sudo rm -rf build
 mkdir build
 cd build
 if (( $debug )); then
  cmake -DCMAKE_BUILD_TYPE=Debug ../ || exit 1
 else
  cmake ../ || exit 1
 fi
fi
sudo make install || exit 1
echo "=================================="
echo "done build krembot_sim/krembot"
echo "=================================="

#build krembot_ws
echo "=================================="
echo "build krembot-ws"
echo "=================================="
cd ~/krembot_ws/build
if (( $clean ))
then
 cd ~/krembot_ws
 sudo rm -rf build
 mkdir build
 cd build
 if (( $debug )); then
  cmake -DCMAKE_BUILD_TYPE=Debug ../ || exit 1
 else
  cmake ../ || exit 1
 fi
fi
make || exit 1
echo "=================================="
echo "done build krembot-ws"
echo "=================================="

unset ARGOS_PLUGIN_PATH

echo ""
echo "=================================="
echo "     build ended successfully"
echo "=================================="

